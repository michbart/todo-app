import React from 'react';
import TodoDatagrid from './components/ui/TodoDatagrid';
import TodoToolbar from './components/ui/TodoToolbar';

function App() {
  return (
    <div>
      <TodoDatagrid />
    </div>
  );
}

export default App;

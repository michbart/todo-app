import React from 'react';
import TodoList from './TodoList';
import TodoForm from './TodoForm';

interface Props {

}

interface State {
  items: []
}


class TodoDatagrid extends React.Component<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
    }
    this.addItem = this.addItem.bind(this);
    this.editItem = this.editItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  componentDidMount() {
    this.fetchItems();

  }

  fetchItems() {
    console.log('fetching');
    // push to items
  }

  addItem() {
    console.log('add item')
    const item  = { id: Date.now(), title: 'foobar', content: 'bar bar bar bar bar' };
    // this.setState(prevState => {
    //   return prevState.items.concat(item)
    // })
    // needs to be checked and refactored
    this.setState({
      items: (this.state.items as any).concat(item)
    });
    console.log(this.state)
  }

  editItem(id) {
    console.log('edit',id)
  }

  deleteItem(id) {
    console.log('delete', id)
  }

  render() {
    return(
      <div>
        <TodoForm addItem={this.addItem}/>
        <TodoList items={this.state.items} editItem={this.editItem} deleteItem={this.deleteItem} />
      </div>
    );
  }
}

export default TodoDatagrid;

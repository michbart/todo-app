import React from 'react';
import { FormControl, InputLabel, Input, FormHelperText, createStyles, withStyles, InputBase, TextField, Card } from '@material-ui/core';
import AddButton from './buttons/AddButon';

const styles = createStyles({
  main: {
    margin: '20px',
    display: 'block',
    background: '#ccc'
  }
})

interface Props {
  addItem?: any,
  classes?: any,
}

class TodoForm extends React.Component<Props> {
  constructor(props){
    super(props);
  }

  addItem() {
    this.props.addItem();
  }

  render() {
    const { classes, ...rest} = this.props;
    return (
      <div>
        <Card className={classes.main}>
        <FormControl >
          <TextField id="title" label="Title" />
          <TextField id="content" label="Content" />
          <AddButton addItem={this.props.addItem}/>
        </FormControl>
        </Card>
      </div>
    );
  }
  
}

export default withStyles(styles)(TodoForm);

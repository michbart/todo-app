import React from 'react';
import EditButton from './buttons/EditButton';
import DeleteButton from './buttons/DeleteButton';


function TodoItemActionbar(props) {
  return (
    <div style={{ float: 'right' }}>
      <EditButton editItem={props.editItem} {...props} />
      <DeleteButton deleteItem={props.deleteItem} {...props} />
    </div>
  );
}

export default TodoItemActionbar;

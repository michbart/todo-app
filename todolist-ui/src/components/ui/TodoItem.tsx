import React from 'react';
import { createStyles, withStyles } from '@material-ui/core/styles';
import EditButton from './buttons/EditButton';
import DeleteButton from './buttons/DeleteButton';
import TodoItemActionbar from './TodoItemActionbar';
import { CardHeader, Card, CardContent } from '@material-ui/core';

const styles = createStyles({
  main: {
    minHeight: '100px',
    margin: '20px',
    display: 'block',
    backgroundColor: '#DCDCDC'
  }
});

interface Props {
  classes?: any,
  content?: any,
  title?: any,
  editItem?: any,
  deleteItem?: any,
  id?: any,
}

class TodoItem extends React.Component<Props> {
  constructor(props) {
    super(props);
    console.log(props)
  }

  render() {
    const { classes, ...rest } = this.props;
    return (
      <div >
        <Card className={classes.main}>
          <CardHeader 
            action={<TodoItemActionbar editItem={this.props.editItem} deleteItem={this.props.deleteItem} {...this.props}/>}
            title={this.props.title}
          />
          <CardContent>
            {this.props.content}
          </CardContent>
        </Card>
      </div>
    );
  }
  
}

export default withStyles(styles)(TodoItem);

import React from 'react';
import { Button, IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

interface Props {
  editItem?: any,
  id?: any,
}

class EditButton extends React.Component<Props> {
  constructor(props) {
    super(props)
    //console.log(props)
    this.editItem = this.editItem.bind(this);
  }

  editItem() {
    //console.log(this.props)
    this.props.editItem(this.props.id);
  }

  render() {
    return (
      <IconButton aria-label="edit" color="primary" onClick={this.editItem}>
        <EditIcon />
      </IconButton>
    );
  }
}

export default EditButton;

import React from 'react';
import { Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

interface Props {
  addItem?: any,
}

class AddButton extends React.Component<Props> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Button 
        variant="contained" 
        color="primary"
        startIcon={<AddIcon />}
        onClick={this.props.addItem}
      >
        Add item
      </Button>
    );
  }

}

export default AddButton;

import React from 'react';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

interface Props {
  deleteItem?: any,
  id?: any,
}

class DeleteButton extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.deleteItem = this.deleteItem.bind(this)
  }

  deleteItem() {
    this.props.deleteItem(this.props.id);
  }

  render() {
    return (
      <IconButton aria-label="delete" color="primary" onClick={this.deleteItem}>
        <DeleteIcon />
      </IconButton>
    );
  }
}

export default DeleteButton;

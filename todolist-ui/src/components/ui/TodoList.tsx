import React from 'react';
import TodoItem from './TodoItem';

interface Props {
  items?: Item[],
  editItem?: any,
  deleteItem?: any,
}

interface Item {
  id: number,
  title: string,
  text: string
}

class TodoList extends React.Component<Props> {

  createItems(props) {
    let items = [] as any;
    props.items.map(item => {
      console.log(item)
      return items.push(<TodoItem {...item} editItem={props.editItem} deleteItem={props.deleteItem}/>);
    })
    return items;
  }

  render() {
    return (
      this.createItems(this.props)
    );
  }
}

export default TodoList;
